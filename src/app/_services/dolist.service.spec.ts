import { TestBed } from '@angular/core/testing';

import { DolistService } from './dolist.service';

describe('DolistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DolistService = TestBed.get(DolistService);
    expect(service).toBeTruthy();
  });
});
