import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DolistService {

  API_URL = 'http://localhost:8000';
  HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});


  constructor(private httpClient: HttpClient) {
  }

  getList(): Observable<any> {
    return this.httpClient.get(this.API_URL + '/list/',
      {headers: this.HttpHeaders});
  }

  createList(list): Observable<any> {
    const body = {list_name: list.list_name, is_done: list.is_done, created_at: list.created_at};
    return this.httpClient.post(this.API_URL + '/list/', body,
      {headers: this.HttpHeaders});
  }

  getSingleList(id): Observable<any> {
    return this.httpClient.get(this.API_URL + '/list/' + id + '/',
      {headers: this.HttpHeaders});
  }

  updateList(list): Observable<any> {
    const body = {list_name: list.list_name, is_done: list.is_done, created_at: list.created_at};
    return this.httpClient.put(this.API_URL + '/list/' + list.id + '/', body,
      {headers: this.HttpHeaders});
  }

  deleteList(id): Observable<any> {
    return this.httpClient.delete(this.API_URL + '/list/' + id + '/',
      {headers: this.HttpHeaders});
  }
}
