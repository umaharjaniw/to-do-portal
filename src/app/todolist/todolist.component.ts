import {Component, OnInit} from '@angular/core';
import {DolistService} from '../_services/dolist.service';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css'],
  providers: [DolistService]

})
export class TodolistComponent implements OnInit {

  List = [{}];
  selectedList;
  private lists;
  // id;
  // list_name;
  // is_done;
  // created_at;


  constructor(private app: DolistService, private router: Router) {
    this.getList();
    this.selectedList = {id: -1, list_name: '', is_done: '', created_at: ''};

  }

  ngOnInit() {
  }

  getList = () => {
    this.app.getList().subscribe(
      data => {
        this.List = data;
      },
      error => {
        console.log(error);
      }
    );
  };

  listClicked = (list) => {
    console.log(list.id);
    this.app.getSingleList(list.id).subscribe(
      data => {
        this.selectedList = data;
        // this.is_done = data.is_done;
        // this.created_at = data.created_at;
      },
      error => {
        console.log(error);
      }
    );

  };

  createList = () => {
    this.app.createList(this.selectedList).subscribe(
      data => {
        this.lists.push(data);
        // this.is_done = data.is_done;
        // this.created_at = data.created_at;
      },
      error => {
        console.log(error);
      }
    );

  };


  updateList = () => {
    this.app.updateList(this.selectedList).subscribe(
      data => {
        this.getList();
        // this.is_done = data.is_done;
        // this.created_at = data.created_at;
      },
      error => {
        console.log(error);
      }
    );

  };

  deleteList = () => {
    this.app.deleteList(this.selectedList.id).subscribe(
      data => {
        this.getList();
        // this.is_done = data.is_done;
        // this.created_at = data.created_at;
      },
      error => {
        console.log(error);
      }
    );

  };
}
